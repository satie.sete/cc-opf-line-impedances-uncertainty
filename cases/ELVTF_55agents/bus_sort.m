function [bus_order, bus_depth, upstream_line, upstream_bus, ...
    downstream_line, downstream_bus] = bus_sort(lines)
% determines the order to update the buses. 
% To compute a backward-forward current flow, end buses need to be updated
% first. 

% input : 
%   lines : matrix describing lines interconnections
%     lines(i,:) = [upstream bus, downstream bus, impedance, power limit] of line i
% output : 
%   bus_order : list of sorted buses
%   bus_depth : depth of a bus in the grid
%   upstream_line : matrix storing the number of the upstream line for each bus 
%   upstream_bus : matrix storing the number of the upstream bus for each bus 
%   downstream_line : cell array storing the numbers of the downstream lines for each bus 
%   downstream_bus : cell array storing the numbers of the downstream buses for each bus 

tmp = lines(:,1:2);
N = max(tmp(:)); % number of buses
[L,~] = size(lines);
depth_max = L;

%% bus depth and upstream line
bus_depth = inf*ones(depth_max+1,N);
bus_depth(:,1) = 0;
upstream_line = zeros(depth_max+1,N);
for d = 2:depth_max+1
    for l = 1:L
        bus_depth(d,lines(l,2)) = bus_depth(d-1,lines(l,1)) + 1;
        upstream_line(d,lines(l,2)) = l;
    end
end
bus_depth = bus_depth(end,:);
upstream_line = upstream_line(end,:);

upstream_bus = zeros(1,N);
for n = 2:N
    upstream_bus(n) = lines(upstream_line(n),1);
end

%% downstream line
downstream_line = cell(N,1);
downstream_bus = cell(N,1);
for n = 1:N
    downstream_line{n} = find(lines(:,1)==n);
    tmp = [];
    for m = 1:length(downstream_line{n})
        tmp = [tmp ; lines(downstream_line{n}(m),2)];
    end
    downstream_bus{n} = tmp;
end

%% bus order
bus_order = zeros(1,N);
tmp = 0;
for d = depth_max:-1:0
    ind = find(bus_depth==d);
    bus_order(tmp+1:tmp+length(ind)) = ind;
    tmp = tmp+length(ind);
end

end