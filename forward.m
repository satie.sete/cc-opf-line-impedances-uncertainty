function Vr = forward(Vs, Z, I)
% load flow algorithm forward step (from supply bus to end buses)
% probabilistic version
% update of voltages 
% All quantities are described by a vector listing their quantile values. 

% inputs
%   Vs : voltage of supplier bus (complex)
%   Z : line impedance (complex)
%   I : line current (complex)
% output
%   Vr : voltage of receiving bus (complex)

Vr = Vs - I.*Z;

end