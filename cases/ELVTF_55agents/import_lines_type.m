function Lines = import_lines_type(filename, dataLines)
%IMPORTFILE Import data from a text file
%  LINES = IMPORTFILE(FILENAME) reads data from text file FILENAME for
%  the default selection.  Returns the data as a string array.
%
%  LINES = IMPORTFILE(FILE, DATALINES) reads data for the specified row
%  interval(s) of text file FILENAME. Specify DATALINES as a positive
%  scalar integer or a N-by-2 array of positive scalar integers for
%  dis-contiguous row intervals.
%
%  Example:
%  Lines = import_lines_type("/home/lglroman/Dropbox/SATIE/PSCC2024/codes/opf/PSO/ELVTF/European_LV_CSV/Lines.csv", [3, Inf]);
%
%  See also READMATRIX.
%
% Auto-generated by MATLAB on 28-Jul-2023 13:32:12

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [3, Inf];
end

%% Set up the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 7);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["Var1", "Var2", "Var3", "Var4", "Var5", "Var6", "VarName7"];
opts.SelectedVariableNames = "VarName7";
opts.VariableTypes = ["string", "string", "string", "string", "string", "string", "string"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Specify variable properties
opts = setvaropts(opts, ["Var1", "Var2", "Var3", "Var4", "Var5", "Var6", "VarName7"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["Var1", "Var2", "Var3", "Var4", "Var5", "Var6", "VarName7"], "EmptyFieldRule", "auto");

% Import the data
Lines = readmatrix(filename, opts);

end