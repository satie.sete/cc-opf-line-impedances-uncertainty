function Is = backward(Ir, V, bus_load)
% load flow algorithm backward step (from end buses to supply bus)
% update of line currents
% inputs   
%   Ir : vector of line currents (complex) coming from downstream buses
%   V : bus voltage bus (complex)
%   bus_load : bus consumption data
%     bus(1) : load impedance
%     bus(2) : consummed power
% output
%   Is : line current (complex) coming from upstream bus

I_bus = V/bus_load(1) + conj(bus_load(2)./V);
Is = sum(Ir,2) + I_bus;

end