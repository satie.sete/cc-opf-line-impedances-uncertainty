function [total_cost,V,P_agent,P_line] = calc_pf_cost(...
    x,bus_agent,variable_buses,...
    lines,lines_values,...
    bus_order,upstream_line,upstream_bus,downstream_line,epsilon)
% comptutes a power flow algorithm according the various impedances
% scenario, then deduces the value of chance constrained OPF utility
% function (including relaxes constraints). 

% inputs:
%   x: power injection plan
%   bus_agent: properties of agents
%   variable_bus: number of buses where a variable agent is connected
%   lines: properties of lines
%   lines_values: values of the impedance of each line according to each
%      scenario
%   bus_order: evaluation order of buses (current flow algorithm)
%   upstream_line: for each bus, number of upstream line
%   upstream_bus: for each bus, number of upstream bus
%   downtream_line: for each bus, numbers of downstream lines
%   epsilon: violation rate of the chance constrained program
% outputs:
%   total_cost: value of the utility function
%   V: voltage at each bus according to each scenario
%   P_agent: power exchange at each bus with the agents, according to each
%      scenario
%   P_line: power flow in each line, according to each scenario

N = length(bus_agent);
bus_power = zeros(N,1);
B = length(variable_buses);
if length(x)==B
    % only active power
    bus_power(variable_buses) = x';
else
    % active and reactive power
    bus_power(variable_buses) = x(1:N)' + 1i*x(N+1:end)';
end
[L,M] = size(lines_values);

%% power flow computation
[V,I,P_agent,P_line] = power_flow(bus_power,lines,lines_values,bus_order,...
    upstream_line,upstream_bus,downstream_line);

%% objective function calculation
cost = zeros(N,1);
if real(P_agent(1))<=0 % PCC injecte dans le réseau
    cost(1) = bus_agent(1,end-1)*abs(mean(real(P_agent(1,:))));
end
for n = 2:N
    cost(n) = bus_agent(n,end-1)*abs(mean(real(P_agent(n,:))));
end
agent_cost = sum(cost);

%% voltage constraints validation
pass_V = zeros(N,1);
for n = 1:N
    pass_V(n) = calc_cc_penalty(abs(V(n,:)), epsilon, bus_agent(n,7), bus_agent(n,8), 0.5*230, 1.5*230);
end
V_constraint = sum(pass_V);

%% line constraints validation
pass_L = zeros(M,1);
for l = 1:L
    pass_L(l) = calc_cc_penalty(abs(P_line(l,:)), epsilon, 0, lines(l,3), 0, 2*lines(l,3));
end
L_constraint = sum(pass_L);

%% total cost
total_cost = agent_cost + V_constraint + L_constraint;
end