the codes of this repository allow to reproduce the results presented in the following reference: Le Goff Latimier, R., Ben Ahmed, H., Chance Constrainted Optimal Power Flow Regarding Line Impedances Uncertainty, submitted to Power System Computational Conference PSCC 2024, Paris Saclay. 

The implementation prioritizes code readability and expandability over performance. 
