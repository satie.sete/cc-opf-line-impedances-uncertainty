function val = calc_cc_penalty(data, epsilon, lb, ub, lrb, urb)
% computes costs associated with relaxed chance constraints.

% inputs:
%   data : vector of data realizations
%   epsilon : tolerated probability of non respect of the chance constraint
%   lb : lower bound of chance constrained set
%   ub : upper bound of chance constrained set
%   lrb : lower bound of robust set
%   urb : upper bound of robust set
% outputs:
%   val: value of relaxed constraints

a = max(...
	max(0,data - ub),...
	max(0,lb - data));
b = (a~=0);
if mean(b)<epsilon
    val_cc = 0;
else
    val_cc = 1e20*sum(a);
end

a = max(...
	max(0,data - urb),...
	max(0,lrb - data));
val_rob = 1e20*sum(a);

val = val_cc + val_rob;
end