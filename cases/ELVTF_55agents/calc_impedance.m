function Z = calc_impedance(length,line_type,Line_impedance)

switch line_type
    case '2c_.007'
        Z = length*(Line_impedance(1,1)+1i*Line_impedance(1,2));
    case '2c_.0225'
        Z = length*(Line_impedance(2,1)+1i*Line_impedance(2,2));
    case '2c_16'
        Z = length*(Line_impedance(3,1)+1i*Line_impedance(3,2));
    case '35_SAC_XSC'
        Z = length*(Line_impedance(4,1)+1i*Line_impedance(4,2));
    case '4c_.06'
        Z = length*(Line_impedance(5,1)+1i*Line_impedance(5,2));
    case '4c_.1'
        Z = length*(Line_impedance(6,1)+1i*Line_impedance(6,2));
    case '4c_.35'
        Z = length*(Line_impedance(7,1)+1i*Line_impedance(7,2));
    case '4c_185'
        Z = length*(Line_impedance(8,1)+1i*Line_impedance(8,2));
    case '4c_70'
        Z = length*(Line_impedance(9,1)+1i*Line_impedance(9,2));
    case '4c_95_SAC_XC'
        Z = length*(Line_impedance(10,1)+1i*Line_impedance(10,2));
end
end