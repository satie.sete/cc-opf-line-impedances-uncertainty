% based on European Low Voltage Test Feeders
% creates a case usable for chance constrained optimal power flow
% the case study is simplified to keep only buses where agents are 
% connected, or buses used as a junction between three or more lines. 

clearvars
close all
clc

%% import of raw data of European Low Voltage Test Feeder case
Lines_full = import_lines_connections("European_LV_Test_Feeder_v2/European_LV_CSV/Lines.csv", [3, Inf]);
Lengths_full = import_lines_length("European_LV_Test_Feeder_v2/European_LV_CSV/Lines.csv", [3, Inf]);
Loads_full = import_loads("European_LV_Test_Feeder_v2/European_LV_CSV/Loads.csv", [4, Inf]);
Types_full = import_lines_type("European_LV_Test_Feeder_v2/European_LV_CSV/Lines.csv", [3, Inf]);
Impedances_type = import_impedance("European_LV_Test_Feeder_v2/European_LV_CSV/LineCodes.csv", [3, Inf]);
Impedances_type = Impedances_type./1000; % km to m conversion
LinesMaxCurrent = import_max_current("European_LV_Test_Feeder_v2/European_LV_CSV/LinesMaxCurrent.csv", [3, 12]);
Lines_capacity = 230*LinesMaxCurrent(1); % choice : the case is considered as single phase. 
Buscoords_full = import_bus_coord("European_LV_Test_Feeder_v2/European_LV_CSV/Buscoords.csv", [3, Inf]);

% number of lines of the full case
[L_full,~] = size(Lines_full);
% number of buses of the full case
N_full = max(max(Lines_full(:,1)),max(Lines_full(:,2)));

%% simplification of intermediate buses
Lines_simple = Lines_full(1,:);
Lengths_simple = Lengths_full(1);
Z_simple = calc_impedance(Lengths_simple,Types_full(1),Impedances_type);
% correspondance table : this line of the full case is embedded in k-th
% line of simplified case
Lines_full2simple = zeros(L_full,1);
Lines_full2simple(1) = 1;
% loop on lines
for l = 2:L_full
    % is there a load on this bus ?
    ind = find(Lines_full(l,1)==Loads_full);
    if isempty(ind) % no
        % is this bus a spur ?
        ind = find(Lines_full(l,1)==Lines_full(:,1));
        if length(ind) == 1
            % no : the bus does not appear in the simplified case.
            ind = find(Lines_simple(:,2)==Lines_full(l,1));
            Lines_simple(ind,2) = Lines_full(l,2);
            Lengths_simple(ind) = Lengths_simple(ind) + Lengths_full(l);
            Z_simple(ind) = Z_simple(ind) + ...
                calc_impedance(Lengths_full(l),Types_full(l),Impedances_type);
            Lines_full2simple(l) = ind;
        else % yes : the bus is added to the simplified case. 
            Lines_simple = [Lines_simple ; Lines_full(l,:)];
            Lengths_simple = [Lengths_simple ; Lengths_full(l)];
            Z_simple = [Z_simple ; calc_impedance(Lengths_full(l),Types_full(l),Impedances_type)];
            Lines_full2simple(l) = length(Lines_simple);
        end
    else % yes : the bus is added to the simplified case. 
        Lines_simple = [Lines_simple ; Lines_full(l,:)];
        Lengths_simple = [Lengths_simple ; Lengths_full(l)];
        Z_simple = [Z_simple ; calc_impedance(Lengths_full(l),Types_full(l),Impedances_type)];
        Lines_full2simple(l) = length(Lines_simple);
    end
end
Capacity_simples = Lines_capacity*ones(length(Lines_simple),1);

%% renumbering of buses and load connections
L_simple = length(Lines_simple);
Lines_simples_renum = zeros(size(Lines_simple));
Buscoords_full = [Buscoords_full , zeros(N_full,1)];
i = 1;
for n = 1:max(Lines_simple(:))
    ind = find(Lines_simple==n);
    if isempty(ind)==0
        Lines_simples_renum(ind) = i;
        Buscoords_full(n,4) = i;
        i = i+1;
    end
end
Loads_simple = zeros(size(Loads_full));
for k = 1:length(Loads_full)
    Loads_simple(k) = Buscoords_full(Loads_full(k),4);
end
N_simple = max(Buscoords_full(:,4));
Buscoords_simple = zeros(N_simple,4);
for k = 1:N_simple
    Buscoords_simple(k,1) = k;
    ind = find(Buscoords_full(:,4)==k);
    Buscoords_simple(k,2:3) = Buscoords_full(ind,2:3);
    Buscoords_simple(k,4) = ind;
end

%% constitution of simplified test case
V_nom = 230;

lines = zeros(L_simple,5);
% [from bus, to bus, capacity, complex impedance, std impedance]
for l = 1:L_simple
    lines(l,:) = [Lines_simples_renum(l,:), Capacity_simples(l), Z_simple(l) 0];
end

M = 1000; % number of sampling for MonteCarlo misestimation of impedances
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lines(:,5) = 0.10; % impedances standard deviation
distrib = 'norm'; % exp, norm, weib
reproductibility = 0; % 0 for new unreproducible random case
                      % 1 for loading a seed (reproducible case)
                      % -1 for generating a seed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lines_values = zeros(L_simple,M);
switch reproductibility
    case 1 % reproducible test case from agent seed 
        % needs to run code with reproducibility=1 beforehand. 
        str = strcat('seed_',distrib);
        load(str,'seed');
        for l = 1:L_simple
            switch distrib
                case 'norm'
                    % trunctated normal distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*seed);
                case 'exp'
                    % exponential distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*seed);
                case 'weib'
                    % weibull distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*seed);
                case 'weib2'
                    % weibull distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*seed);
            end
        end
    case 0 % unreproducible test case
        for l = 1:L_simple
            switch distrib
                case 'norm'
                    % trunctated normal distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*abs(randn(M,1))/0.6);
                case 'exp'
                    % exponential distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*exprnd(1,M,1));
                case 'weib'
                    % weibull distribution
                    lines_values(l,:) = lines(l,4)*(1+lines(l,5)*wblrnd(5,1,M,1)/5);
            end
        end
    case -1 % seed generation for lines
        switch distrib
            case 'norm'
                seed = abs(randn(M,1))/0.6;
                save('seed_norm','seed')
            case 'exp'
                seed = exprnd(1,M,1);
                save('seed_exp','seed')
            case 'weib'
                seed = wblrnd(5,1,M,1)/5;
                save('seed_weib','seed')
            case 'weib2'
                seed = wblrnd(5,2,M,1)/2.22;
                save('seed_weib2','seed')
        end
end

[bus_order, bus_depth, upstream_line, upstream_bus, ...
    downstream_line, downstream_bus] = bus_sort(lines);

bus_agents = zeros(N_simple,10);
% bus_agent(i,:) = [connected to bus , P_max , P_min, Q_max, Q_min, ...
% Z_load, v_min, v_max, a(1), a(2)]
% a(1), a(2) : cost function parameters
% cost = a(1)*P + a(2)*P^2
epexspot2022 = import_epex_spot("epex_spot_2022.csv", [2, 8618]);
a = mean(epexspot2022)/1000; % conversion from euro/Wh
bus_agents(1,:) = [1, Inf, -Inf, Inf, -Inf, Inf, ...
        V_nom, V_nom, a, 0];
% producer
P_prod = 1e5;
% consummers
P_cons_max = 1e5;
P_cons_min = 0;
switch reproductibility
    case 1 % reproducible test case from agent seed 
        % needs to run code with reproducibility=1 beforehand. 
        load('seed_agents');
        for n = 2:N_simple
            if sum(n==Loads_simple)>0 % an agent is connected to this bus
                if seed_agents(n,1)<0.5 % producer
                    bus_agents(n,:) = ...
                        [n, 0, -P_prod, 0, 0, Inf, 0.95*V_nom, 1.05*V_nom, ...
                        0.06 + 0.01*seed_agents(n,2), 0];
                else % consumer
                    bus_agents(n,:) = ...
                        [n, P_cons_max, P_cons_min, 0, 0, ...
                        Inf, 0.95*V_nom, 1.05*V_nom, -0.5 + 0.1*seed_agents(n,2), 0];
                end
            else
                bus_agents(n,:) = [n, 0, 0, 0, 0, Inf, ...
                    0.95*V_nom, 1.05*V_nom, 0, 0];
            end
        end
    case 0 % unreproducible test case
        for n = 2:N_simple
            if sum(n==Loads_simple)>0 % an agent is connected to this bus
                tmp = rand();
                if tmp<0.5 % producer
                    bus_agents(n,:) = ...
                        [n, 0, -P_prod, 0, 0, Inf, 0.95*V_nom, 1.05*V_nom, ...
                        0.06 + 0.01*randn(), 0];
                else % consumer
                    bus_agents(n,:) = ...
                        [n, P_cons_max, P_cons_min, 0, 0, ...
                        Inf, 0.95*V_nom, 1.05*V_nom, -0.5 + 0.1*randn(), 0];
                end
            else
                bus_agents(n,:) = [n, 0, 0, 0, 0, Inf, ...
                    0.95*V_nom, 1.05*V_nom, 0, 0];
            end
        end
    case -1 % seed generation for agents
        seed_agents = randn(N_simple,2);
        save('seed_agents',"seed_agents")
end

% list of buses with a variable power input
variable_buses = Loads_simple;

%% saving test case
str = strcat('ELVTF_55agents_',distrib,'_',num2str(100*lines(1,5)));
save(str,'bus_agents','lines','lines_values','variable_buses',...
    'Buscoords_simple','bus_order','bus_depth','upstream_bus',...
    'upstream_line','downstream_bus','downstream_line')

%% drawing of simplified and full case
figure
hold on
axis equal
for n = 1:N_full
    plot(Buscoords_full(n,2),Buscoords_full(n,3),'ro')
end
for l = 1:L_full
    X = [Buscoords_full(Lines_full(l,1),2) ; Buscoords_full(Lines_full(l,2),2)];
    Y = [Buscoords_full(Lines_full(l,1),3) ; Buscoords_full(Lines_full(l,2),3)];
    plot(X,Y,'k')
end
figure
hold on
axis equal
for l = 1:L_simple
    X = [Buscoords_simple(Lines_simples_renum(l,1),2) ; Buscoords_simple(Lines_simples_renum(l,2),2)];
    Y = [Buscoords_simple(Lines_simples_renum(l,1),3) ; Buscoords_simple(Lines_simples_renum(l,2),3)];
    plot(X,Y,'k')
end
for n = 1:N_simple
    if sum(n==variable_buses)>0
        plot(Buscoords_simple(n,2),Buscoords_simple(n,3),'bo')
    else
        plot(Buscoords_simple(n,2),Buscoords_simple(n,3),'ro')
    end
end