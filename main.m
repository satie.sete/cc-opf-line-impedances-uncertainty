%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Resolution of a chance constrained optimal power flow by %
% particle swarm optimisation.                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This open source code allows to reproduce results presented in
% Le Goff Latimier, R. Ben Ahmed, H., 'Chance Constrained Optimal Power
% Flow Regarding Line Impedances Uncertainty, submitted to Power System
% Computation Conference PSCC 2024, Paris Saclay

% requires the following toolboxes
%   global optimization toolbox

clearvars
close all
clc

%% parameters of the chance constrained program
sigma = 0.10; % uncertainty on impedances lines
epsilon = 0.05; % probability of non compliance of chance constraint
dist = 'norm'; % distribution of probability describing impedances misestimations
M = 50; % number of samples of MonteCarlo (must be less than 1000 or modify test case). 

%% load of case parameters
str = strcat('cases/ELVTF_55agents/ELVTF_55agents_',dist,'_',num2str(100*sigma),'.mat');
load(str);
lines_values = lines_values(:,1:M);

%% utility function to optimize and parameters
fun = @(P)(calc_pf_cost(P,bus_agents,variable_buses,...
    lines,lines_values,...
    bus_order,upstream_line,upstream_bus,downstream_line,...
    epsilon));
N = length(variable_buses);

%% paramters of swarm optimisation %
size_swarm = 50;
max_iter = 20;

%% initial swarm
% the initial swarm is made up of particles where all consumers have the 
% same power P_cons, and all producers have the same power P_prod. These
% two powers sweep a grid between their minimum and maximum permissible 
% values. 

K = 10; % discretisation parameter (K^2 must be greater than size_swarm)
init_pop = zeros(K*K,N);
for i = 1:K % loop on producers' power
    for j = 1:K % loop on consumers' power
        ind = K*(i-1)+j;
        for n = 1:N % for each bus
            k = variable_buses(n);
            if (bus_agents(k,2)~=0)||(bus_agents(k,3)~=0)
                if bus_agents(k,2)==0 % if producer
                    init_pop(ind,n) = ((i-1)/K)*bus_agents(k,3);
                elseif bus_agents(k,2)==Inf % if pcc
                    init_pop(ind,n) = 0;
                else % if consumer
                    init_pop(ind,n) = bus_agents(k,3) +...
                        ((j-1)/K)*(bus_agents(k,2) - bus_agents(k,3));
                end
            end
        end
    end
end

% evaluation of initial swarm utility fonction
init_cost = zeros(K*K,1);
parfor i = 1:K*K
    init_cost(i) = fun(init_pop(i,:));
end

% selection of best individual
[init_cost,ind] = sort(init_cost);
init_pop = init_pop(ind,:);
init_swarm = init_pop(1:size_swarm,:);
init_cost = init_cost(1:size_swarm);

%% main optimisation call
lb = bus_agents(variable_buses,3);
ub = bus_agents(variable_buses,2);
options = optimoptions('particleswarm',...
    'SwarmSize',size_swarm, ...
    'InitialSwarmMatrix',init_swarm,... 
    'PlotFcn','pswplotbestf',...
    'Display','iter', ...
    'MaxIterations',max_iter, ...
    'MaxTime',8*3600, ... % maximum duration of the optimisation in seconds
    'UseParallel',true);
nvars = length(variable_buses);
[x,fval,exitflag,output] = ...
    particleswarm(fun,nvars,lb,ub,options);

%% post processing of optimal result
[total_cost,V,P_agent,P_line] = calc_pf_cost(...
    x,bus_agents,variable_buses,lines,lines_values,bus_order,...
    upstream_line,upstream_bus,downstream_line,epsilon);

%% saving of results
str = strcat('cases/ELVTF_55agents/results_exp_sigma',num2str(100*sigma),'_eps',num2str(epsilon*100));
save(str)