function epexspot2022 = import_epex_spot(filename, dataLines)
%IMPORTFILE Import data from a text file
%  EPEXSPOT2022 = IMPORTFILE(FILENAME) reads data from text file
%  FILENAME for the default selection.  Returns the numeric data.
%
%  EPEXSPOT2022 = IMPORTFILE(FILE, DATALINES) reads data for the
%  specified row interval(s) of text file FILENAME. Specify DATALINES as
%  a positive scalar integer or a N-by-2 array of positive scalar
%  integers for dis-contiguous row intervals.
%
%  Example:
%  epexspot2022 = import_epex_spot("/home/lglroman/Dropbox/SATIE/PSCC2024/codes/opf/PSO/ELVTF/epex_spot_2022.csv", [2, 8618]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 29-Jul-2023 13:22:31

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [2, 8618];
end

%% Set up the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 5);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["Var1", "Var2", "Var3", "price_euros_mwh", "Var5"];
opts.SelectedVariableNames = "price_euros_mwh";
opts.VariableTypes = ["string", "string", "string", "double", "string"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Specify variable properties
opts = setvaropts(opts, ["Var1", "Var2", "Var3", "Var5"], "WhitespaceRule", "preserve");
opts = setvaropts(opts, ["Var1", "Var2", "Var3", "Var5"], "EmptyFieldRule", "auto");

% Import the data
epexspot2022 = readtable(filename, opts);

%% Convert to output type
epexspot2022 = table2array(epexspot2022);
end