function [V,I,P_agent,P_line] = power_flow(...
    bus_power, lines, lines_values, ...
    bus_order, upstream_line, upstream_bus,downstream_line)
% computes the power flow in a radial network, with an iterative load flow
% algorithm. 

% inputs:
%   bus_power: power injection plan at each bus
%   lines: properties of lines
%   lines_values: values of the impedance of each line according to each
%      scenario
%   bus_order: evaluation order of buses (current flow algorithm)
%   upstream_line: for each bus, number of upstream line
%   upstream_bus: for each bus, number of upstream bus
%   downtream_line: for each bus, numbers of downstream lines
% outputs:
%   V: voltage at each bus according to each scenario
%   I: current in each line according to each scenario
%   P_agent: power exchange at each bus with the agents, according to each
%      scenario
%   P_line: power flow in each line, according to each scenario

[N,~] = size(bus_power);
[L,~] = size(lines);
[~,M] = size(lines_values); % number of cases for Monte Carlo simulation

tmp = zeros(N,2);
tmp(:,1) = Inf*ones(N,1);
tmp(:,2) = bus_power;
bus_power = tmp;

%% convergence tolerance
tol = 1e-6;
K_max = 100; % maximum number of iterations

%% variables preallocation
V = zeros(K_max,N,M); % voltages
I = zeros(K_max,L,M); % line currents
res = zeros(K_max,N,M,2); % residuals

%% initialisation
V0 = 230;
V(:,1,:) = V0;
V(1,:,:) = V0;

%% main loop
for k = 2:K_max
    % backward step
    for i_n = 1:N
        n = bus_order(i_n);
        if isempty(downstream_line{n})
            I(k,upstream_line(n),:) = backward(...
                0, squeeze(V(k-1,n,:)), bus_power(n,:));
        elseif upstream_line(n)~=0
            I(k,upstream_line(n),:) = backward(...
                I(k,downstream_line{n},:), ...
                V(k-1,n,:), bus_power(n,:));
        end
    end
    % forward step
    for i_n = N:-1:1
        n = bus_order(i_n);
        for m = 1:length(downstream_line{n})
            V(k,lines(downstream_line{n}(m),2),:) = forward(...
                squeeze(V(k,n,:)), ...
                lines_values(downstream_line{n}(m),:)', ...
                squeeze(I(k,downstream_line{n}(m),:)));
        end
    end
    % convergence check
    for n = 1:N
        tmp = squeeze(V(k,n,:) - V(k-1,n,:));
        res(k-1,n,1) = norm(tmp);
    end
    for l = 1:L
        tmp = squeeze(I(k,n-1,:) - I(k-1,n-1,:));
        res(k-1,l,2) = norm(tmp);
    end
    if abs(squeeze(res(k-1,:,:)))<tol
        break
    end
end

%% post treatment
V = squeeze(V(k,:,:));
I = squeeze(I(k,:,:));
% power of each agent and in each line
P_agent = zeros(N,M);
P_line = zeros(L,M);
for i_n = 1:N
    n = bus_order(i_n);
    if isempty(downstream_line{n})
        P_agent(n,:) = conj(V(n,:)).*I(upstream_line(n),:);
        P_line(upstream_line(n),:) = conj(V(upstream_bus(n),:)).*I(upstream_line(n),:);
    elseif upstream_line(n)~=0
        P_agent(n,:) = conj(V(n,:)).*I(upstream_line(n),:) - sum(P_line(downstream_line{n},:),1);
        P_line(upstream_line(n),:) = conj(V(upstream_bus(n),:)).*I(upstream_line(n),:);
    else % pcc bus
        P_agent(n,:) = - sum(P_line(downstream_line{n},:),1);
    end
end
end